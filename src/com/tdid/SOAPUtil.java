package com.tdid;

import java.text.SimpleDateFormat;
import java.util.Iterator;

import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;

public class SOAPUtil {
	public static final String NS_PREFIX_EB = "eb";
	public static final String NS_URI_EB = "http://www.oasis-open.org/committees/ebxml-msg/schema/msg-header-2_0.xsd";
	public static final String NS_PREFIX_XLINK = "xlink";
	public static final String NS_URI_XLINK = "http://www.w3.org/1999/xlink";
	public static final String NS_PREFIX_XSI = "xsi";
	public static final String NS_URI_XSI = "http://www.w3.org/2001/XMLSchema-instance";

	public static void ebSetup(SOAPEnvelope envelope, String frparty, String toparty, String cpaId, String conversId,
			String service, String action) {
		try {
			SOAPHeader header = envelope.getHeader();
			//header.setAttribute("xsi:schemaLocation", "http://www.oasis-open.org/committees/ebxml-msg/schema/msg-header-2_0.xsd http://www.oasis-open.org/committees/ebxml-msg/schema/msg-header-2_0.xsd");
			SOAPHeaderElement ebHeader = header.addHeaderElement(envelope.createName("MessageHeader",NS_PREFIX_EB, NS_URI_EB));
			ebHeader.setMustUnderstand(true);
			ebHeader.addAttribute(envelope.createName("eb:version"), "2.0");
			SOAPElement ebFrom = ebHeader.addChildElement(envelope.createName("From", NS_PREFIX_EB, NS_URI_EB));
			SOAPElement ebPartyFrom = ebFrom.addChildElement(envelope.createName("PartyId", NS_PREFIX_EB, NS_URI_EB));
			ebPartyFrom.setAttribute("eb:type", "string");
			ebPartyFrom.addTextNode(frparty);
			SOAPElement ebRoleFrom = ebFrom.addChildElement(envelope.createName("Role", NS_PREFIX_EB, NS_URI_EB));
			//ebRoleFrom.setAttribute("type", "simple");
			ebRoleFrom.addTextNode("Provider");
			SOAPElement ebTo = ebHeader.addChildElement(envelope.createName("To", NS_PREFIX_EB, NS_URI_EB));
			SOAPElement ebPartyTo = ebTo.addChildElement(envelope.createName("PartyId", NS_PREFIX_EB, NS_URI_EB));
			ebPartyTo.setAttribute("eb:type", "string");
			ebPartyTo.addTextNode(toparty);
			SOAPElement ebRoleTo = ebTo.addChildElement(envelope.createName("Role", NS_PREFIX_EB, NS_URI_EB));
			//ebRoleTo.setAttribute("type", "simple");
			ebRoleTo.addTextNode("THRD");
			SOAPElement ebCPAId = ebHeader.addChildElement(envelope.createName("CPAId", NS_PREFIX_EB, NS_URI_EB));
			ebCPAId.setAttribute("eb:type", "string");
			ebCPAId.addTextNode(cpaId);
			SOAPElement ebConv = ebHeader
					.addChildElement(envelope.createName("ConversationId", NS_PREFIX_EB, NS_URI_EB));
			ebConv.addTextNode(conversId);
			SOAPElement ebService = ebHeader.addChildElement(envelope.createName("Service", NS_PREFIX_EB, NS_URI_EB));
			ebService.setAttribute("eb:type", "string");
			ebService.addTextNode(service);
			SOAPElement ebAction = ebHeader.addChildElement(envelope.createName("Action", NS_PREFIX_EB, NS_URI_EB));
			ebAction.addTextNode(action);
			SOAPElement ebMesData = ebHeader
					.addChildElement(envelope.createName("MessageData", NS_PREFIX_EB, NS_URI_EB));
			SOAPElement ebMesId = ebMesData.addChildElement(envelope.createName("MessageId", NS_PREFIX_EB, NS_URI_EB));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HHmmss-SSSSS");
			String formattedTime = sdf.format(System.currentTimeMillis());
			ebMesId.addTextNode(formattedTime + "@192.168.6.17");
			SOAPElement ebMesTime = ebMesData
					.addChildElement(envelope.createName("Timestamp", NS_PREFIX_EB, NS_URI_EB));
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
			ebMesTime.addTextNode(sdf2.format(System.currentTimeMillis()));
			
			SOAPHeaderElement ebSyncReply = header.addHeaderElement(envelope.createName("SyncReply", NS_PREFIX_EB, NS_URI_EB));
			//ebSyncReply.setAttribute("eb:id", "ebMS2MshSignalChannel_A01");
			ebSyncReply.setAttribute("xmlns:eb", "http://www.oasis-open.org/committees/ebxml-msg/schema/msg-header-2_0.xsd");
			ebSyncReply.setAttribute("eb:version", "2.0");
			ebSyncReply.setAttribute("SOAP-ENV:mustUnderstand", "1");
			ebSyncReply.setAttribute("SOAP-ENV:actor", "http://schemas.xmlsoap.org/soap/actor/next");
			SOAPHeaderElement ebAckRequested = header.addHeaderElement(envelope.createName("AckRequested", NS_PREFIX_EB, NS_URI_EB));
			ebAckRequested.setAttribute("xmlns:eb", "http://www.oasis-open.org/committees/ebxml-msg/schema/msg-header-2_0.xsd");
			ebAckRequested.setAttribute("eb:version", "2.0");
			ebAckRequested.setAttribute("SOAP-ENV:mustUnderstand", "1");
			ebAckRequested.setAttribute("SOAP-ENV:actor", "urn:oasis:names:tc:ebxml-msg:actor:nextMSH");
			ebAckRequested.setAttribute("eb:signed", "false");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static SOAPElement getChild(SOAPEnvelope envelope, SOAPElement elem, String child) throws SOAPException {
		Iterator<?> iter = elem.getChildElements(envelope.createName(child, NS_PREFIX_EB, NS_URI_EB));
		if (iter.hasNext()) {
			SOAPElement e = (SOAPElement) iter.next();
			return e;
		} else
			return null;
	}
}

package com.tdid;

import javax.xml.soap.*;

import javax.xml.soap.SOAPMessage;


public class SendQueryServlet {
	
	public static void callSoapWebService(String soapEndpointUrl) {
        try {
            // Create SOAP Connection
        	SOAPConnection soapConn = SOAPConnectionFactory.newInstance().createConnection();
            //SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            //SOAPConnection soapConnection = soapConnectionFactory.createConnection();
            SOAPRequest soaprequest = new SOAPRequest();
            // Send SOAP Message to SOAP Server
            SOAPMessage soapRequest = soaprequest.createSOAPRequest();
            SOAPMessage soapResponse = soapConn.call(soapRequest, soapEndpointUrl);

            // Print the SOAP Response
            System.out.println("Response SOAP Message:");
            soapResponse.writeTo(System.out);
            System.out.println();

            soapConn.close();
            
        } catch (Exception e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
    }
	
}

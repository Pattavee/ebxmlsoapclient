package com.tdid;

import java.io.File;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

public class SOAPRequest {

	public SOAPMessage createSOAPRequest() {
		try {
			MessageFactory mf = MessageFactory.newInstance();
			SOAPMessage msg = mf.createMessage();
			SOAPPart sp = msg.getSOAPPart();
			sp.setContentId("www.etaxsolution.com");
			// create the header container
			SOAPEnvelope envelope = sp.getEnvelope();
			envelope.setAttribute("xmlns:"+SOAPUtil.NS_PREFIX_XLINK, SOAPUtil.NS_URI_XLINK);
			envelope.setAttribute("xmlns:"+SOAPUtil.NS_PREFIX_XSI, SOAPUtil.NS_URI_XSI);
			envelope.setAttribute("xmlns:"+SOAPUtil.NS_PREFIX_EB, SOAPUtil.NS_URI_EB);
			envelope.setAttribute("xsi:schemaLocation", "http://schemas.xmlsoap.org/soap/envelope/ http://www.oasis-open.org/committees/ebxml-msg/schema/envelope.xsd http://www.oasis-open.org/committees/ebxml-msg/schema/msg-header-2_0.xsd http://www.oasis-open.org/committees/ebxml-msg/schema/msg-header-2_0.xsd");
			SOAPUtil.ebSetup(envelope, "0105531071981", "THRD", "THRD_ETAXINVOICE_0105531071981_SP_TEST_1.00_cf", "TT01055310719815A663111111137",
					"THRD.eTaxInvoice", "REQ_RESPONSE");
			/*SOAPUtil.ebSetup(envelope, "0107536000315", "THRD", "THRD_ETAXINVOICE_0107536000315_SP_TEST_1.00_4c", "TT01075360003155A563000000005",
					"THRD.eTaxInvoice", "TIV");*/
			SOAPBody body = envelope.getBody();
			//body.setAttribute("xsi:schemaLocation", "http://www.oasis-open.org/committees/ebxml-msg/schema/msg-header-2_0.xsd http://www.oasis-open.org/committees/ebxml-msg/schema/msg-header-2_0.xsd");
			SOAPElement manifest = body
					.addBodyElement(envelope.createName("Manifest", SOAPUtil.NS_PREFIX_EB, SOAPUtil.NS_URI_EB));
			manifest.addAttribute(envelope.createName("eb:version"), "2.0");
			SOAPElement reference = manifest
					.addChildElement(envelope.createName("Reference", SOAPUtil.NS_PREFIX_EB, SOAPUtil.NS_URI_EB));
			reference.addAttribute(envelope.createName("href", SOAPUtil.NS_PREFIX_XLINK, SOAPUtil.NS_URI_XLINK),
					"cid:www.etaxsolution.com");
			reference.addAttribute(envelope.createName("type", SOAPUtil.NS_PREFIX_XLINK, SOAPUtil.NS_URI_XLINK),
					"simple");
			SOAPElement description = reference
					.addChildElement(envelope.createName("Description", SOAPUtil.NS_PREFIX_EB, SOAPUtil.NS_URI_EB));
			description.addAttribute(envelope.createName("xml:lang"), "en-US");
			description.addTextNode("Service Provider");
			// create the payload container
			
			//an instance of factory that gives a document builder  
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();  
			//an instance of builder to parse the specified xml file  
			DocumentBuilder db = dbf.newDocumentBuilder();  
			//Parse the content to Document object
	        Document xmlDocument = db.parse(new File("./documents/TaxInvoice.xml"));
			
			TransformerFactory tf = TransformerFactory.newInstance();
		    Transformer transformer = tf.newTransformer();
			
			StringWriter sw = new StringWriter();
			//transform document to string 
	        transformer.transform(new DOMSource(xmlDocument), new StreamResult(sw));
	        
			AttachmentPart transferAttach = msg.createAttachmentPart(sw.toString(), "text/xml;charset=utf-8");
			transferAttach.setContentType("text/xml;charset=utf-8");
			transferAttach.setContentId("www.etaxsolution.com");
			msg.addAttachmentPart(transferAttach);
			msg.setProperty(SOAPMessage.WRITE_XML_DECLARATION, "true");
			// Writing the message to file to see the SOAPMessage.
			msg.writeTo(System.out);
			System.out.println();
			return msg;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
